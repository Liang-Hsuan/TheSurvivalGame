# The Survival Game #
## A fun online 2D multiplayer survival game | our 1st web game ##

[go to play game](http://www.survivalgameonline.com)

**The main function of this web game:**

* it allows user to create his or her own room and player for the game.

* the user can play with other players simply giving others the room name, so that other users can add in to the game as well. 

* the goal is to achieve as high score as possible, so it is important to stay alive during the game as long as possible.

-------------

**The safty of this web game:**

* in order to play this game, the user does not need to create an account or fill in any personal information.

* the user can check any necessary status of the room and delete it, so that it will clear the cookies.

-------------

**Third party libraries**

* Firebase (for keeping track players' state) & Phaser (for 2D gaming animation)

-------------


**Note:**
We only created a foundation for a fun online survival game, if you are interested to make it better or add more features, it is welcome to fork the repo.